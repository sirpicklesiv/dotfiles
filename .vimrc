" Set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" Disable vi compatability
set nocompatible

" Auto indentation
set autoindent

" Smart indentatino for C
set smartindent

" Configure tabs
set tabstop=4
set shiftwidth=4
set expandtab

" Wrap line width
"set textwidth=120

" Turn syntax hightlighting on
syntax on

" Turn linenumbers on
set number

